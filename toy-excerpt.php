<?php

/**
 * Plugin Name:       Toy Excerpt
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-excerpt
 * Description:       Helps customize excerpts and read more links.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! function_exists( 'toy_excerpt_more' ) ) :
	function toy_excerpt_more( $data ) {
		if ( empty( trim( $data ) ) ) {
			return;
		}

		return sprintf(
			'<span class="read-more"><a class="read-more-link" href="%1$s">%2$s</a></span>',
			get_permalink( get_the_ID() ),
			__( 'Read More &rsaquo;' )
		);
	}
endif;

add_filter(
	'excerpt_length',
	function( $l ) {
		$with = get_option( 'excerpt' ) || 26;

		return $with;
	},
	67
);

add_filter(
	'the_content_more_link',
	function( $link ) {
		return preg_replace( '|#more-[0-9]+|', '', $link );
	},
	67
);

add_filter(
	'get_the_excerpt',
	function( $data ) {
		$data = str_replace( '[&hellip;]', '', $data );
		$data = trim( $data, ' ' );

		if ( ! is_attachment() ) {
			$data .= toy_excerpt_more( $data );
		}

		return $data;
	},
	67
);

add_filter(
	'the_excerpt',
	function( $data ) {
		if ( post_password_required() ) {
			// Show exception for password protected content
			$data = __( 'This is a password protected entry' ) . generic_excerpt_more();
		}

		return $data;
	},
	67
);
